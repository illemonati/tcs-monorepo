export const API_BASE =
    process.env.REACT_APP_API_BASE || "https://tcs-api.r2h.us/";

export const WS_BASE = process.env.REACT_APP_WS_BASE || "ws://10.0.0.29:14433/";

export const drawerWidth = 240;
