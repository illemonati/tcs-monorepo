export const parseTitle = () => {
    const title = document.title;
    return [...title.matchAll(/(.+)(\s\|\sTCS)/g)]?.[0]?.[1] || title;
};
