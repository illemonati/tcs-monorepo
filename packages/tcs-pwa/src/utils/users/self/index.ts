import axios from "axios";

export const getSelf = async () => {
    const resp = await axios.get("users/self");
    return resp.data;
};
