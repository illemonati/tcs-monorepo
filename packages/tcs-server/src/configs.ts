import os from "os";
import path from "path";

export const tempFileFolder =
    process.env.TEMP_FILE_FOLDER || path.join(os.tmpdir(), "/tcs-temp");
export const fileStorageBaseDir =
    process.env.FILE_STORAGE_BASE_DIR || path.join(os.tmpdir(), "/tcs-files");

export const whiteListedMimeTypes = (mimeType: string) => {
    if (mimeType.startsWith("video/")) return true;
    if (mimeType.startsWith("image/")) return true;
    return false;
};

export const maxFileNameByteLength = 255;
