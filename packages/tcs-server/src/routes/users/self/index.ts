import express from "express";
import { prisma } from "@tcs/tcs-prisma";
import { getSelf } from "../../../utils/users";

const router = express.Router();

router.get("/", async (req, res) => {
    const accessHash = req.get("Authorization");
    if (!accessHash) {
        return res.status(403).end();
    }

    const self = await getSelf(accessHash);
    return res.send(self);
});

router.post("/change-display-name", async (req, res) => {
    const AccessHash = req.get("Authorization");
    if (!AccessHash) {
        return res.status(403).end();
    }
    const newDisplayName = req.body["new-display-name"];
    if (!newDisplayName) {
        return res.status(400).send({ message: "missing new display name" });
    }
    const user = await prisma.user.update({
        where: {
            AccessHash,
        },
        data: {
            DisplayName: newDisplayName,
        },
    });
    if (!user) {
        return res.status(400).send({ message: "user not found" });
    }
    return res.status(200).send(user);
});

export default router;
