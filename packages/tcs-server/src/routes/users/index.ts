import express from "express";
import { prisma } from "@tcs/tcs-prisma";
import self from "./self";

const router = express.Router();

router.use("/self", self);

export default router;
