import express from "express";
import channel from "./channel";
import create from "./create";

const router = express.Router();

router.use("/channel", channel);
router.use("/create", create);

export default router;
