import express from "express";
import { prisma } from "@tcs/tcs-prisma";
import { createRandomHash } from "../../../utils/hash";

const router = express.Router();

router.post("/", async (req, res) => {
    const accessHash = req.get("Authorization");

    if (!accessHash) {
        return res.status(403).end();
    }

    const { name, description, collectiveHash } = req.body;

    if (!name || !description || name.length < 3 || name.length > 25) {
        return res.status(400).end();
    }

    const channelPublicHash = createRandomHash();

    try {
        const user = await prisma.user.findUnique({
            where: {
                AccessHash: accessHash,
            },
            select: {
                PublicHash: true,
            },
        });

        if (!user) {
            return res.status(403).end();
        }

        const member = await prisma.member.findUnique({
            where: {
                userPublicHash_collectivePublicHash: {
                    userPublicHash: user.PublicHash,
                    collectivePublicHash: collectiveHash,
                },
            },
            select: {
                IsAdministrator: true,
            },
        });

        if (!member) {
            return res.status(400).end();
        }

        if (!member.IsAdministrator) {
            return res.status(403).end();
        }

        const channel = await prisma.channel.create({
            data: {
                Name: name,
                Description: description,
                HasIcon: false,
                PublicHash: channelPublicHash,
                Collective: {
                    connect: {
                        PublicHash: collectiveHash,
                    },
                },
                DisplayOrder: 0,
            },
        });
        res.status(200).send(channelPublicHash);
    } catch (e) {
        let resp = "";
        if (typeof e === "string") {
            resp = e.toUpperCase(); // works, `e` narrowed to string
        } else if (e instanceof Error) {
            resp = e.message; // works, `e` narrowed to Error
        }
        res.status(500).send(resp);
    }
});

export default router;
