import express from "express";
import { prisma } from "@tcs/tcs-prisma";
import { authForChannel } from "../../../utils/auth";
import crypto from "crypto";
import { sha3_256 } from "js-sha3";
import { channelEventEmitter } from "../../../utils/events";
import type { Message } from "@tcs/tcs-prisma";
import { processAttachmentFiles, upload } from "../../../utils/files";
import { createRandomHash } from "../../../utils/hash";
import voice from "./voice";

const router = express.Router();

router.use("/:PublicHash/voice", voice);

router.post(
    "/:PublicHash/message-create",
    upload.fields([{ name: "attachments", maxCount: 50 }]),
    async (req, res) => {
        const accessHash = req.get("Authorization");

        const content: string = req.body.content;

        if (!accessHash) {
            return res.status(403).end();
        }

        if (!content && !req.files) {
            return res.status(300).end();
        }

        const messagePublicHash = createRandomHash();

        const files = req.files as {
            [fieldname: string]: Express.Multer.File[];
        } | null;

        // const attachmentFiles = [] as Express.Multer.File[];

        const attachmentFiles = (files && files["attachments"]) || [];

        const attachmentData = (
            await processAttachmentFiles(attachmentFiles)
        ).filter((data) => !!data);

        const PublicHash = req.params.PublicHash;

        const member = await authForChannel(PublicHash, accessHash);

        if (!member) return res.status(403).end();

        const message = await prisma.message.create({
            data: {
                Channel: {
                    connect: {
                        PublicHash,
                    },
                },
                Author: {
                    connect: {
                        PublicHash: member.PublicHash,
                    },
                },
                Content: content,
                PublicHash: messagePublicHash,
                Attachments: {
                    createMany: {
                        data: attachmentData,
                    },
                },
            },
            include: {
                Author: {
                    select: {
                        User: {
                            select: {
                                PublicHash: true,
                                DisplayName: true,
                            },
                        },
                    },
                },
                Attachments: true,
            },
        });

        channelEventEmitter.emit("message-create", PublicHash, message);

        return res.send(message);
    }
);

router.ws("/:PublicHash/live-feed", async (ws, req) => {
    const accessHash: string = await new Promise((r) => {
        ws.on("message", (msg) => {
            r(msg.toString());
        });
    });

    if (!accessHash) {
        ws.send("Unauthorized");
        ws.close();
    }

    const PublicHash = req.params.PublicHash;

    const member = await authForChannel(PublicHash, accessHash);
    if (!member) {
        ws.send("Unauthorized");
        ws.close();
    }

    const messageCreateHandler = (publicHash: string, message: Message) => {
        if (publicHash !== PublicHash) return;
        ws.send(
            JSON.stringify({
                event: "message-create",
                data: { message: message },
            })
        );
    };

    channelEventEmitter.on("message-create", messageCreateHandler);

    ws.on("close", () => {
        channelEventEmitter.removeListener(
            "message-create",
            messageCreateHandler
        );
    });
});

router.get("/", async (req, res) => {
    res.status(400).end();
});

router.get("/:PublicHash", async (req, res) => {
    const accessHash = req.get("Authorization");
    if (!accessHash) {
        return res.status(403).end();
    }

    const PublicHash = req.params.PublicHash;

    if (!(await authForChannel(PublicHash, accessHash)))
        return res.status(403).end();

    const channel = await prisma.channel.findUnique({
        where: {
            PublicHash,
        },
    });

    return res.send(channel);
});

router.get("/:PublicHash/channel-messages", async (req, res) => {
    const accessHash = req.get("Authorization");
    if (!accessHash) {
        return res.status(403).end();
    }

    const PublicHash = req.params.PublicHash;

    if (!(await authForChannel(PublicHash, accessHash)))
        return res.status(403).end();

    const messages = await prisma.channel.findUnique({
        where: {
            PublicHash,
        },
        include: {
            Messages: {
                include: {
                    Author: {
                        include: {
                            User: {
                                select: {
                                    PublicHash: true,
                                    DisplayName: true,
                                },
                            },
                        },
                    },
                    Attachments: true,
                },
            },
        },
    });

    return res.send(messages);
});

export default router;
