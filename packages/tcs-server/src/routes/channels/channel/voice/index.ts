import express from "express";
import { authForChannel } from "../../../../utils/auth";
const router = express.Router({ mergeParams: true });
import { WebSocket } from "ws";
import { createRandomHash } from "../../../../utils/hash";

router.get("/", (req, res) => {
    return res.json(req.params);
});

interface VoiceChannelsWithMembers {
    [VoiceChannelPublicHash: string]: {
        [MemberPublicHash: string]: {
            PublicHash: string;
            WS: WebSocket;
            ConnectionID: string;
        };
    };
}

interface ReceivedMessage {
    event: string;
    CounterParty: string;
    ConnectionID: string;
    data?: any;
}

const vToM: VoiceChannelsWithMembers = {};

router.ws("/join", async (ws, req) => {
    const accessHash: string = await new Promise((r) => {
        ws.on("message", (msg) => {
            r(msg.toString());
        });
    });

    if (!accessHash) {
        ws.send("Unauthorized");
        ws.close();
        return;
    }

    const channelPublicHash = req.params.PublicHash;

    const member = await authForChannel(channelPublicHash, accessHash);

    if (!member) {
        ws.send("Unauthorized");
        ws.close();
        return;
    }

    if (!(channelPublicHash in vToM)) {
        vToM[channelPublicHash] = {};
    }

    const channel = vToM[channelPublicHash];

    const handleCloseConnection = (connID: string) => {
        for (let otherChannelMember of Object.values(channel)) {
            otherChannelMember.WS.send(
                JSON.stringify({
                    event: "close-connection",
                    CounterParty: member.PublicHash,
                    ConnectionID: connID,
                })
            );
        }
    };

    if (member.PublicHash in channel) {
        channel[member.PublicHash].WS.close();
        const connID = channel[member.PublicHash].ConnectionID;
        delete channel[member.PublicHash];
        handleCloseConnection(connID);
    }

    const ConnectionID = createRandomHash();

    channel[member.PublicHash] = {
        PublicHash: member.PublicHash,
        WS: ws,
        ConnectionID,
    };
    ws.on("close", () => {
        if (
            !(member.PublicHash in channel) ||
            channel[member.PublicHash].ConnectionID !== ConnectionID
        ) {
            return;
        }

        delete channel[member.PublicHash];

        handleCloseConnection(ConnectionID);
    });

    const channelMember = channel[member.PublicHash];

    for (let otherChannelMember of Object.values(channel)) {
        if (otherChannelMember.ConnectionID === ConnectionID) break;

        ws.send(
            JSON.stringify({
                event: "request-offer",
                CounterParty: otherChannelMember.PublicHash,
                ConnectionID: otherChannelMember.ConnectionID,
            })
        );
    }

    ws.on("message", (e) => {
        const messageRaw = e.toString();
        // console.log(messageRaw.startsWith("{"), messageRaw.endsWith("}"));
        if (!messageRaw.startsWith("{") || !messageRaw.endsWith("}")) {
            return;
        }
        const message: ReceivedMessage = JSON.parse(messageRaw);

        const CounterParty = channel[message.CounterParty];

        if (!CounterParty) return;

        console.log(CounterParty.ConnectionID, message.ConnectionID);

        if (CounterParty?.ConnectionID !== message?.ConnectionID) {
            return;
        }

        console.log(message);

        if (message.event === "offer-created") {
            const offer = message.data?.offer;
            if (offer) {
                CounterParty.WS.send(
                    JSON.stringify({
                        event: "offer-delivery",
                        CounterParty: channelMember.PublicHash,
                        ConnectionID: channelMember.ConnectionID,
                        data: {
                            offer,
                        },
                    })
                );
            }
        } else if (message.event === "answer-created") {
            const answer = message.data?.answer;
            if (answer) {
                CounterParty.WS.send(
                    JSON.stringify({
                        event: "answer-delivery",
                        CounterParty: channelMember.PublicHash,
                        ConnectionID: channelMember.ConnectionID,
                        data: {
                            answer,
                        },
                    })
                );
            }
        } else if (message.event === "icecandidate-created") {
            const icecandidate = message.data?.icecandidate;
            console.log(message);
            if (icecandidate) {
                CounterParty?.WS?.send(
                    JSON.stringify({
                        event: "icecandidate-delivery",
                        CounterParty: channelMember.PublicHash,
                        ConnectionID: channelMember.ConnectionID,
                        data: {
                            icecandidate,
                        },
                    })
                );
            }
        }
    });
});

export default router;
