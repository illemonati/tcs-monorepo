import express from "express";
import nocache from "nocache";
import attachments from "./attachments";
import channels from "./channels";
import collectives from "./collectives";
import users from "./users";

const router = express.Router();

router.use("/collectives", collectives);
router.use("/users", users);
router.use("/channels", channels);

router.use("/attachments", attachments);

module.exports = router;
