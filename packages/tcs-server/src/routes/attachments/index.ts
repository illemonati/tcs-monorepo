import express from "express";
import attachment from "./attachment";

const router = express.Router();

router.use("/attachment", attachment);

export default router;
