import express from "express";
import { getAttachmentPath, sendFile } from "../../../utils/files";

const router = express.Router();

router.get("/:PublicHash/:Name", async (req, res, next) => {
    try {
        const { PublicHash, Name } = req.params;
        if (!PublicHash || !Name) return res.status(404).end();
        const filePath = getAttachmentPath(PublicHash, Name);
        await sendFile(req, res, filePath);
    } catch (e) {
        next();
    }
});

export default router;
