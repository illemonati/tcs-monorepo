import express from "express";
import { prisma } from "@tcs/tcs-prisma";
import { sha3_256 } from "js-sha3";
import crypto from "crypto";
import { authForCollective } from "../../../utils/auth";
import { createRandomHash } from "../../../utils/hash";

const router = express.Router();

router.get("/:PublicHash", async (req, res) => {
    const accessHash = req.get("Authorization");
    if (!accessHash) {
        return res.status(403).end();
    }

    const PublicHash = req.params.PublicHash;

    const collective = await prisma.collective.findUnique({
        where: {
            PublicHash,
        },
    });

    return res.send(collective);
});

router.get("/:PublicHash/collective-channels", async (req, res) => {
    const accessHash = req.get("Authorization");
    const collectivePublicHash = req.params.PublicHash;

    const member = await authForCollective(collectivePublicHash, accessHash);

    if (!member) return res.status(403).end();

    const collective = await prisma.collective.findUnique({
        where: {
            PublicHash: collectivePublicHash,
        },
        include: {
            Channels: true,
        },
    });

    return res.send({ collective, member });
});

router.post("/:PublicHash/join", async (req, res) => {
    const accessHash = req.get("Authorization");
    if (!accessHash) {
        return res.status(403).end();
    }

    const collectivePublicHash = req.params.PublicHash;

    try {
        const user = await prisma.user.findUnique({
            where: {
                AccessHash: accessHash,
            },
            select: {
                PublicHash: true,
            },
        });

        if (!user) {
            return res.status(403).end();
        }

        await prisma.member.upsert({
            where: {
                userPublicHash_collectivePublicHash: {
                    userPublicHash: user.PublicHash,
                    collectivePublicHash: collectivePublicHash,
                },
            },
            create: {
                PublicHash: createRandomHash(),
                IsAdministrator: false,
                Collective: {
                    connect: {
                        PublicHash: collectivePublicHash,
                    },
                },
                User: {
                    connect: {
                        PublicHash: user.PublicHash,
                    },
                },
            },
            update: {},
        });
        res.send("success");
    } catch (e) {
        let resp = "";
        if (typeof e === "string") {
            resp = e.toUpperCase(); // works, `e` narrowed to string
        } else if (e instanceof Error) {
            resp = e.message; // works, `e` narrowed to Error
        }
        res.status(500).send(resp);
    }
});

export default router;
