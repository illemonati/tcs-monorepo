import express from "express";
import collective from "./collective";
import self from "./self";
import create from "./create";

const router = express.Router();

router.use("/collective", collective);
router.use("/create", create);
router.use("/self", self);

export default router;
