import express from "express";
import { prisma } from "@tcs/tcs-prisma";

const router = express.Router();

router.get("/", async (req, res) => {
    const accessHash = req.get("Authorization");
    if (!accessHash) {
        return res.status(403).end();
    }

    const user = await prisma.user.findUnique({
        where: {
            AccessHash: accessHash,
        },
        select: {
            Member: {
                select: {
                    Collective: true,
                },
            },
        },
    });

    if (user == null) return res.status(403).end();

    const members = user.Member;
    const collectives = members
        .filter((member) => member.Collective != null)
        .map((member) => member.Collective);

    return res.send(collectives);
});

export default router;
