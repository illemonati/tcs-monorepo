import express from "express";
import { prisma } from "@tcs/tcs-prisma";
import { createRandomHash } from "../../../utils/hash";

const router = express.Router();

router.post("/", async (req, res) => {
    const accessHash = req.get("Authorization");

    if (!accessHash) {
        return res.status(403).end();
    }

    const { name, description } = req.body;

    if (!name || !description || name.length < 3 || name.length > 25) {
        return res.status(400).end();
    }

    const collectivePublicHash = createRandomHash();

    try {
        const collective = await prisma.collective.create({
            data: {
                Name: name,
                Description: description,
                HasIcon: false,
                SubCollectiveDisplayOrder: 0,
                PublicHash: collectivePublicHash,
                Members: {
                    create: {
                        PublicHash: createRandomHash(),
                        User: {
                            connect: {
                                AccessHash: accessHash,
                            },
                        },
                        IsAdministrator: true,
                    },
                },
            },
        });
        res.status(200).send(collectivePublicHash);
    } catch (e) {
        let resp = "";
        if (typeof e === "string") {
            resp = e.toUpperCase(); // works, `e` narrowed to string
        } else if (e instanceof Error) {
            resp = e.message; // works, `e` narrowed to Error
        }
        res.status(500).send(resp);
    }
});

export default router;
