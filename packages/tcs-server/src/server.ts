import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import expressWs from "express-ws";
import compression from "compression";
import { fileStorageBaseDir, tempFileFolder } from "./configs";
import nocache from "nocache";
// import routes from "./routes";

const { app, getWss, applyTo } = expressWs(express(), undefined, {
    leaveRouterUntouched: false,
});

const routes = require("./routes");

app.disable("x-powered-by");

// parse application/json
app.use(bodyParser.json());

// nocache
app.use(nocache());

// cors
app.use(cors());

// compression
app.use(compression());

const port = process.env.PORT || 14433;

app.get("/", (req, res) => {
    res.send("TCS SERVER!");
});

app.use("/", routes);

app.listen(port, () => {
    console.log(`tempFileFolder: ${tempFileFolder}`);
    console.log(`fileStorageBaseDir: ${fileStorageBaseDir}`);
    console.log(`Server is listening on ${port}`);
});
