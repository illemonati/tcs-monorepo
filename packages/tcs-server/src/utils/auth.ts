import { prisma } from "@tcs/tcs-prisma";

export const authForCollective = async (
    collectivePublicHash: string,
    accessHash?: string
) => {
    if (!accessHash) {
        return false;
    }

    const user = await prisma.user.findUnique({
        where: {
            AccessHash: accessHash,
        },
        select: {
            PublicHash: true,
        },
    });

    if (!user) {
        return false;
    }

    const member = await prisma.member.findUnique({
        where: {
            userPublicHash_collectivePublicHash: {
                userPublicHash: user.PublicHash,
                collectivePublicHash: collectivePublicHash,
            },
        },
        select: {
            PublicHash: true,
            IsAdministrator: true,
        },
    });

    return member;
};

export const authForChannel = async (
    channelPublicHash: string,
    accessHash?: string
) => {
    if (!accessHash) return false;

    const channel = await prisma.channel.findUnique({
        where: {
            PublicHash: channelPublicHash,
        },
        select: {
            collectivePublicHash: true,
        },
    });

    if (!channel || !channel.collectivePublicHash) return false;
    return await authForCollective(channel.collectivePublicHash, accessHash);
};
