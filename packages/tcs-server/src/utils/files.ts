import multer from "multer";
import {
    fileStorageBaseDir,
    maxFileNameByteLength,
    tempFileFolder,
} from "../configs";
import { createRandomHash } from "./hash";
import { Request, Response } from "express";
import fs from "fs-extra";
import path from "path";
import mime from "mime-types";
import { whiteListedMimeTypes } from "../configs";
import zlib from "zlib";
import truncate from "truncate-utf8-bytes";
import ffmpeg, { FfprobeData } from "fluent-ffmpeg";

export const upload = multer({ dest: tempFileFolder });

export const getAttachmentPath = (
    attachmentPublicHash: string,
    attachmentName: string
) => {
    return path.join(
        fileStorageBaseDir,
        "attachments/",
        attachmentPublicHash,
        attachmentName
    );
};

export const trimFileName = (name: string) => {
    const byteLength = Buffer.byteLength(name);
    if (byteLength <= maxFileNameByteLength) return name;
    const extension = name.match(/(\.[0-9a-z]+)$/i)?.[0];
    const fileName = extension
        ? name.substring(0, name.length - extension.length)
        : name;
    const byteLengthAllowedForFileName =
        maxFileNameByteLength - (extension?.length || 0);

    if (byteLengthAllowedForFileName < 1) return null;

    const croppedFileName = truncate(fileName, byteLengthAllowedForFileName);
    const final = croppedFileName + extension;
    return final;
};

export const processAttachmentFiles = async (
    attachmentFiles: Express.Multer.File[]
) => {
    const attachments = await Promise.all(
        attachmentFiles.map(async (file) => {
            const attachmentPublicHash = createRandomHash();

            let width = undefined;
            let height = undefined;

            const trimmedFileName =
                trimFileName(file.originalname) || "toolong.bin";

            if (
                ["video", "image"]
                    .map((type) => file.mimetype.startsWith(type))
                    .some((x) => !!x)
            ) {
                try {
                    const data: FfprobeData = await new Promise((r, e) =>
                        ffmpeg.ffprobe(file.path, (err, data) => {
                            err ? e(err) : r(data);
                        })
                    );
                    const vidStreams = data.streams.filter(
                        (s) => s.codec_type === "video"
                    );
                    if (vidStreams.length < 1) {
                        throw new Error("No vid streams");
                    }
                    const stream = vidStreams[0];
                    width = stream.width;
                    height = stream.height;
                } catch (e) {
                    console.log(e);
                    // ignore
                }
            }

            await fs.move(
                file.path,
                getAttachmentPath(attachmentPublicHash, trimmedFileName)
            );
            await fs.remove(file.path);
            return {
                PublicHash: attachmentPublicHash,
                MimeType: file.mimetype,
                Name: trimmedFileName,
                Size: file.size,
                Width: width,
                Height: height,
            };
        })
    );
    return attachments;
};

// Safari fix from https://stackoverflow.com/a/29126190
export const sendFile = async (
    req: Request,
    res: Response,
    filePath: string
) => {
    if (!(await fs.pathExists(filePath))) return res.status(404).end();

    const mimeType = mime.lookup(filePath) || "data/octet-stream";

    const stat = fs.statSync(filePath);

    res.setHeader("Content-Type", mimeType);
    res.setHeader("Accept-Ranges", "bytes");

    if (!whiteListedMimeTypes(mimeType)) {
        res.setHeader("Content-Disposition", "attachment");
    }

    const range = req.headers.range;
    const total = stat.size;

    let start = 0;
    let end = total - 1;

    if (range) {
        const parts = range.replace(/bytes=/, "").split("-");
        const partialStart = parts[0];
        const partialEnd = parts[1];

        if (partialStart) start = parseInt(partialStart, 10);
        if (partialEnd) end = parseInt(partialEnd, 10);

        const chunksize = end - start + 1;

        res.setHeader("Content-Range", `bytes ${start}-${end}/${total}`);
        res.setHeader("Content-Length", chunksize);
        // partial content
        res.status(206);
    } else {
        res.setHeader("Content-Length", total);
    }

    const readStream = fs.createReadStream(filePath, { start, end });

    // if (req.acceptsEncodings(["gzip"])) {
    //     res.setHeader("Content-Encoding", "gzip");
    //     readStream.pipe(zlib.createGzip()).pipe(res);
    // } else {
    //     readStream.pipe(res);
    // }

    readStream.pipe(res);
};
