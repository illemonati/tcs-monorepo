import { prisma } from "@tcs/tcs-prisma";
import { sha3_256 } from "js-sha3";

import {
    uniqueNamesGenerator,
    Config,
    adjectives,
    colors,
    animals,
} from "unique-names-generator";

const nameConfig: Config = {
    dictionaries: [adjectives, colors, animals],
    separator: " ",
    style: "capital",
    length: 3,
};

const newName = () => uniqueNamesGenerator(nameConfig);

export const getSelf = async (AccessHash: string) => {
    const user = await prisma.user.upsert({
        where: {
            AccessHash,
        },
        update: {
            LastLogIn: new Date(),
        },
        create: {
            AccessHash,
            PublicHash: sha3_256(AccessHash),
            DisplayName: newName(),
        },
    });
    return user;
};
