import { sha3_256 } from "js-sha3";
import crypto from "crypto";

export const createRandomHash = () => {
    return sha3_256(crypto.randomBytes(20).toString("hex"));
};
